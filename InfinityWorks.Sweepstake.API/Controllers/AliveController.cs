﻿using Microsoft.AspNetCore.Mvc;

namespace InfinityWorks.Sweepstake.API.Controllers
{
    [Route("")]
    public class AliveController : Controller
    {

        [HttpGet]
        public IActionResult Alive()
        {
            return Ok("I'm alive!!!");
        }
    }
}
